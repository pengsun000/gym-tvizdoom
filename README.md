# gym_tvzidoom
ViZDoom `gym` wrapper with customized environments, 
particularly for the Active Object Tracking tasks discussed in the [following paper](https://sites.google.com/site/whluoimperial/active_tracking_icml2018):  

@inproceedings{luo2018end,  
title={End-to-end Active Object Tracking via Reinforcement Learning},  
author={Luo, Wenhan and Sun, Peng and Zhong, Fangwei and Liu, Wei and Zhang, Tong and Wang, Yizhou},  
booktitle={International Conference on Machine Learning},  
year={2018}  
}  



 

## Installation
The code was tested in *`Python 2.7`*. 
It is strongly recommended that a virutal env (by either `virtualenv` or `conda`)
is used. 
Then git clone the repo, cd to the folder and run:  
``` bash
pip install -e .
```
which should also automatically install the following dependencies:  

* `gym==0.7.4`  
* `vizdoom`  

## How to run
Use the standard `gym.make('env_name')` to make the environment in your training code.

For an example,
cd to the `gym_tvizdoom` and run:
```bash
python test.py
```
you should see a random agent (the tracker) in first person perspective and a moving object in the view. 

Playaround with the environments in `test.py`,
and note that for the env name with `Spectator` suffix you can control the agent by keyboard.

See `gym_tvizdoom/__init__.py` for all available envs (those with `TrackObj` prefix) 

### Familiar with ViZDoom and only interested in *.wad files?
See the folder [`gym_tvizdoom/envs/_track`](https://bitbucket.org/pengsun000/gym-tvizdoom/src/master/gym_tvizdoom/envs/_track/) and check the *.wad (as well as the *.cfg) files therein.

### Looking for randomized/augmented environments?
We implement this by randomly selecting an environment upon starting each episode. 
See an example [`gym_tvizdoom/envs/track_obj_small_maze_rand_v2_env.py`](https://bitbucket.org/pengsun000/gym-tvizdoom/src/master/gym_tvizdoom/envs/track_obj_small_maze_rand_v2_env.py)