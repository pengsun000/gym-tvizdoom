from setuptools import setup

setup(name='gym_tvizdoom',
      version='0.0.1',
      install_requires=[
            'gym==0.7.4',
            'vizdoom',
      ]
)  
