from gym.envs.registration import register


def register_vizdoom_gamevar_rwdshape_envs(basename, version, envclass, max_episode_steps):
    """ register gym env-id as string "basename[Gamevar][Rwdshape][Visible][Spectator]-version"

    e.g., HealthGatherGamevarVisibleSpectator-v0, HealthGather-v0, ...
    """
    for has_gamevar in [True, False]:
        for has_rwdshape in [True, False]:
            for visible in [True, False]:
                for is_spectator in [True, False]:
                    str_gamevar = "Gamevar" if has_gamevar else ""
                    str_rwdshape = "Rwdshape" if has_rwdshape else ""
                    str_visible = "Visible" if visible else ""
                    str_spectator = "Spectator" if is_spectator else ""
                    id = "{}{}{}{}{}-{}".format(basename, str_gamevar, str_rwdshape, str_visible, str_spectator,
                                                version)
                    entry_point = "gym_tvizdoom.envs:{}".format(envclass)

                    register(
                        id=id, entry_point=entry_point,
                        tags={'vizdoom': True},
                        kwargs={'visible': visible, 'has_gamevar': has_gamevar, 'has_rwdshape': has_rwdshape,
                                'is_spectator': is_spectator},
                        max_episode_steps=max_episode_steps,
                    )


def register_vizdoom_envs(basename, version, envclass, max_episode_steps):
    """ register gym env-id as string "basename[Visible][Spectator]-version"

    e.g., HealthGatherVisibleSpectator-v0, HealthGather-v0, ...
    """

    for visible in [True, False]:
        for is_spectator in [True, False]:
            str_visible = "Visible" if visible else ""
            str_spectator = "Spectator" if is_spectator else ""
            id = "{}{}{}-{}".format(basename, str_visible, str_spectator, version)
            entry_point = "gym_tvizdoom.envs:{}".format(envclass)

            register(
                id=id, entry_point=entry_point,
                tags={'vizdoom': True},
                kwargs={'visible': visible, 'has_gamevar': False, 'is_spectator':is_spectator},
                max_episode_steps=max_episode_steps,
            )


def register_vizdoom_gamevar_envs(basename, version, envclass, max_episode_steps):
    """ register gym env-id as string "basename[Gamevar][Visible][Spectator]-version"

    e.g., HealthGatherGamevarVisibleSpectator-v0, HealthGather-v0, ...
    """
    for has_gamevar in [True, False]:
        for visible in [True, False]:
            for is_spectator in [True, False]:
                str_gamevar = "Gamevar" if has_gamevar else ""
                str_visible = "Visible" if visible else ""
                str_spectator = "Spectator" if is_spectator else ""
                id = "{}{}{}{}-{}".format(basename, str_gamevar, str_visible, str_spectator, version)
                entry_point = "gym_tvizdoom.envs:{}".format(envclass)

                register(
                    id=id, entry_point=entry_point,
                    tags={'vizdoom': True},
                    kwargs={'visible': visible, 'has_gamevar': has_gamevar, 'is_spectator':is_spectator},
                    max_episode_steps=max_episode_steps,
                )


# envs with game variables
name_version_envclass = [
    ['HealthGather', 'v0', 'HealthGatherEnv'],  # Health Gather
    ['HealthGatherTwotypes', 'v0', 'HealthGatherTwotypesEnv'],  # Health Gather Two types
    ['CSmall', 'v0', 'CSmallEnv'],  # Combat
    ['CMeet', 'v0', 'CMeetEnv'],  # Meet
    ['CMeetM', 'v0', 'CMeetMEnv'],  # Meet Monster
    ['CChamber', 'v0', 'CChamberEnv'],  # Chamber
    ['CChamber1', 'v0', 'CChamber1Env'],  # Chamber1
    ['CChamber2', 'v0', 'CChamber2Env'],  # Chamber2
    ['CChamber3', 'v0', 'CChamber3Env'],  # Chamber3
    ['CChamber4', 'v0', 'CChamber4Env'],  # Chamber4
    ['CChamber5', 'v0', 'CChamber5Env'],  # Chamber5
    ['CChamber51', 'v0', 'CChamber51Env'],  # Chamber51
    ['CChamberRand', 'v0', 'CChamberRandEnv'],  # Chamber Random
    ['CFlat', 'v0', 'CFlatEnv'],  # Flat

    ['CCig', 'v0', 'CCigEnv'],  # cig
]
max_episode_steps = 2100*2

for name, version, envclass in name_version_envclass:
    register_vizdoom_gamevar_envs(name, version, envclass, max_episode_steps)


# envs with game variables and reward shaping
name_version_envclass = [
    ['CFlat0', 'v0', 'CFlat0Env'],  # Flat0
    ['CFlat1', 'v0', 'CFlat1Env'],  # Flat1
    ['CFlat2', 'v0', 'CFlat2Env'],  # Flat2
    ['CFlat3', 'v0', 'CFlat3Env'],  # Flat3
    ['CFlat4', 'v0', 'CFlat4Env'],  # Flat4
    ['CFlat5', 'v0', 'CFlat5Env'],  # Flat5
    ['CFlat6', 'v0', 'CFlat6Env'],  # Flat6
    ['CFlat7', 'v0', 'CFlat7Env'],  # Flat7
    ['CFlat8', 'v0', 'CFlat8Env'],  # Flat8
    ['CFlatRand', 'v0', 'CFlatRandEnv'],  # Flat Rand

    ['CTest', 'v0', 'CTestEnv'],  # Test
    ['CTest2Wu', 'v0', 'CTest2WuEnv'],  # Test2 Wu
    ['CTest2Greedy', 'v0', 'CTest2GreedyEnv'],  # Test2 Greedy
    ['CTest2ProtGreedy', 'v0', 'CTest2ProtGreedyEnv'],  # Test2 Prot Greedy
    ['CTest3ProtGreedy', 'v0', 'CTest3ProtGreedyEnv'],  # Test3 Prot Greedy

    ['CFlatcig0', 'v0', 'CFlatcig0Env'],  # Flatcig0
    ['CFlatcig1', 'v0', 'CFlatcig1Env'],  # Flatcig1
    ['CFlatcig2', 'v0', 'CFlatcig2Env'],  # Flatcig2
    ['CFlatcig3', 'v0', 'CFlatcig3Env'],  # Flatcig3
    ['CFlatcig4', 'v0', 'CFlatcig4Env'],  # Flatcig4
    ['CFlatcig5', 'v0', 'CFlatcig5Env'],  # Flatcig5
    ['CFlatcig6', 'v0', 'CFlatcig6Env'],  # Flatcig6
    ['CFlatcig7', 'v0', 'CFlatcig7Env'],  # Flatcig7

    ['CRichcig0', 'v0', 'CRichcig0Env'],  # Richcig0
    ['CRichcig1', 'v0', 'CRichcig1Env'],  # Richcig1
    ['CRichcig2', 'v0', 'CRichcig2Env'],  # Richcig2
    ['CRichcig3', 'v0', 'CRichcig3Env'],  # Richcig3
    ['CRichcig4', 'v0', 'CRichcig4Env'],  # Richcig4
    ['CRichcig5', 'v0', 'CRichcig5Env'],  # Richcig5
    ['CRichcig6', 'v0', 'CRichcig6Env'],  # Richcig6
    ['CRichcig7', 'v0', 'CRichcig7Env'],  # Richcig7

    ['CNorthcig0', 'v0', 'CNorthcig0Env'],  # Northcig
    ['CWestcig0', 'v0', 'CWestcig0Env'],  # Westcig
    ['CEastcig0', 'v0', 'CEastcig0Env'],  # Eastcig
    ['CSouthcig0', 'v0', 'CSouthcig0Env'],  # Southcig

    ['CFlatcigRand', 'v0', 'CFlatcigRandEnv'],  # Flatcig Rand
    ['CRichcigRand', 'v0', 'CRichcigRandEnv'],  # RichcigRand
    ['CCigRand', 'v0', 'CCigRandEnv'],  # cig Rand
    ['CCigRand2', 'v0', 'CCigRand2Env'],  # cig Rand2

    ['CFlatcigAttack0', 'v0', 'CFlatcigAttack0Env'],  # Flatcig Attack0
]
max_episode_steps = 2100*2

for name, version, envclass in name_version_envclass:
    register_vizdoom_gamevar_rwdshape_envs(name, version, envclass, max_episode_steps)


# envs without game variables
name_version_envclass = [
    ['TrackObj', 'v2', 'TrackObjV2Env'],  # Track Obj (wenhan's v2)
    ['TrackObjZombie', 'v2', 'TrackObjZombieV2Env'],  # Track Obj another obj Zombie (wenhan's v2)
    ['TrackObjMarine', 'v2', 'TrackObjMarineV2Env'],  # Track Obj another obj Marine (wenhan's v2)
    ['TrackObjMarine2', 'v2', 'TrackObjMarine2V2Env'],  # Track Obj another obj Marine (wenhan's v2)
    ['TrackObjMonsterx', 'v2', 'TrackObjMonsterxV2Env'],  # Track Obj monster x (wenhan's v2)
    ['TrackObjFloorCeil', 'v2', 'TrackObjFloorCeilV2Env'],  # Track Obj background, floor ceil (wenhan's v2)
    ['TrackObjNoise', 'v2', 'TrackObjNoiseV2Env'],  # Track Obj Noise (wenhan's v2)
    ['TrackObjNoise2', 'v2', 'TrackObjNoise2V2Env'],  # Track Obj Noise2 (wenhan's v2)
    ['TrackObjSmallMaze1', 'v2', 'TrackObjSmallMaze1V2Env'],  # Track Obj Small Maze1 (wenhan's v2)
    ['TrackObjSmallMazeRand', 'v2', 'TrackObjSmallMazeRandV2Env'],  # Track Obj Small Maze Rand (wenhan's v2)
    ['TrackObjSmallMazeRandFlip', 'v2', 'TrackObjSmallMazeRandFlipV2Env'],  # Track Obj Small Maze Rand Flip (wenhan's v2)
    ['TrackObjOMaze', 'v2', 'TrackObjOMazeEnv'],  # Track Obj O Maze
    ['TrackObjOMazeCorner', 'v2', 'TrackObjOMazeCornerEnv'],  # Track Obj O Maze Corner
    ['TrackObjCorridor', 'v2', 'TrackObjCorridorEnv'],  # Track Obj Corridor
    ['TrackObjHard', 'v2', 'TrackObjHardV2Env'],  # Track Obj Hard
    ['TrackObjCounterclockwise', 'v2', 'TrackObjCounterclockwiseEnv'],  # Track Obj Counterclockwise
    ['TrackObjBack', 'v2', 'TrackObjBackV2Env'],  # Track Obj Back
    ['TrackObjBack2', 'v2', 'TrackObjBack2V2Env'],  # Track Obj Back2
    ['TrackObjShift', 'v0', 'TrackObjShiftEnv'],  # Track Obj Shift
    ['TrackObjShiftSlowClose', 'v0', 'TrackObjShiftSlowCloseEnv'],  # Track Obj Shift slow close
    ['TrackObjShiftSlowCloseSmallMaze', 'v0', 'TrackObjShiftSlowCloseSmallMazeEnv'],  # Track Obj Shift slow close small
    ['TrackObjShiftSlowCloseSmallMazeRTL', 'v0', 'TrackObjShiftSlowCloseSmallMazeRTLEnv'],  # Track Obj Shift slow close small right to left
    ['TrackObjShiftSlowCloseRD', 'v0', 'TrackObjShiftSlowCloseRDEnv'],  # Track Obj Shift slow close, Reward Difference
]
max_episode_steps = 3002

for name, version, envclass in name_version_envclass:
    register_vizdoom_envs(name, version, envclass, max_episode_steps)


register(
    id='TrackObj-v3',
    entry_point='gym_tvizdoom.envs:TrackObjV3Env',
    tags={
        'vizdoom': True
    },
    kwargs={'visible': False},
    max_episode_steps=3001,
)

register(
    id='TrackObjVisible-v3',
    entry_point='gym_tvizdoom.envs:TrackObjV3Env',
    tags={
        'vizdoom': True
    },
    kwargs={'visible': True},
    max_episode_steps=3001,
)

register(
    id='TrackObjEasy-v0',
    entry_point='gym_tvizdoom.envs:TrackObjEasyEnv',
    tags={
        'vizdoom': True
    },
    kwargs={'visible': False},
    max_episode_steps=3001,
)

register(
    id='TrackObjEasyVisible-v0',
    entry_point='gym_tvizdoom.envs:TrackObjEasyEnv',
    tags={
        'vizdoom': True
    },
    kwargs={'visible': True},
    max_episode_steps=3001,
)