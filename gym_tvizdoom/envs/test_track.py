#!/usr/bin/env python
from __future__ import print_function

import random
from time import sleep

from vizdoom import GameVariable, Mode, doom_fixed_to_double

from track_obj_shift_env import TrackObjShiftEnv

episodes = 3
sleep_time = 0.028
#sleep_time = 0.0


env = TrackObjShiftEnv(visible=True)

action_size = env.action_space.n


for i in range(episodes):
    print("Episode #" + str(i + 1))

    terminal = False
    j = 0
    while not terminal:
        j += 1

        # Makes a random action
        action = random.randint(0, action_size-1)
        state, reward, terminal, _ = env.step(action)


        # internal variables
        def cvt(x):
            tmp = env.game.get_game_variable(x)  # Get value of scripted variable
            return doom_fixed_to_double(tmp)

        x = cvt(GameVariable.USER1)
        y = cvt(GameVariable.USER2)
        a = cvt(GameVariable.USER3)
        xx = cvt(GameVariable.USER4)
        yy = cvt(GameVariable.USER5)
        aa = cvt(GameVariable.USER6)

        print("#iter: ", j)
        print("player x, y, a: ", x, y, a)
        print("target xx, yy, aa: ", xx, yy, aa)
        print("True Reward: ", reward)
        print("Episode Reward: ", env.episode_reward)
        print("=====================")

        # Sleep some time because processing is too fast to watch.
        if sleep_time > 0:
            sleep(sleep_time)

    print("Episode finished!")
    print("************************")
    env.reset()

