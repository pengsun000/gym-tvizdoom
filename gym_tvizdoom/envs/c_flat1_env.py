import logging
from c_base_greedyrs_env import CBaseGreedyrsEnv

GAME_CFG = '_combat/flat1.cfg'
logger = logging.getLogger(__name__)


class CFlat1Env(CBaseGreedyrsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CFlat1Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator)
