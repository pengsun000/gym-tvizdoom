import logging
from c_base_greedyrs_env import CBaseGreedyrsEnv

GAME_CFG = '_combat/flat2.cfg'
logger = logging.getLogger(__name__)


class CFlat2Env(CBaseGreedyrsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CFlat2Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator)
