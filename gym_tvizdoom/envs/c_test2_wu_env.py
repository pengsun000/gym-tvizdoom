import logging
from c_base_mp_wurs_env import CBaseMpWursEnv

GAME_CFG = '_combat/test2.cfg'
logger = logging.getLogger(__name__)


class CTest2WuEnv(CBaseMpWursEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CTest2WuEnv, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame=4)
