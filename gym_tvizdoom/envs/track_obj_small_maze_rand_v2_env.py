import logging
from random import choice

import gym
from gym import spaces

from base_env import BaseEnv
from base_env import SCREEN_SHAPE
from track_obj_v2_env import TrackObjV2Env

REAL_ACTIONS = [
    [True, False, False],
    [False, True, False],
    [True, False, True],
    [False, True, True],
    [False, False, True],
    [False, False, False],
]
GAME_CFG_LIST = [
    '_track/track_obj_small_maze1.cfg',
    '_track/track_obj_small_maze2.cfg',
    '_track/track_obj_small_maze3.cfg',
    '_track/track_obj_small_maze4.cfg',
    '_track/track_obj_small_maze5.cfg',
    '_track/track_obj_small_maze6.cfg',
    '_track/track_obj_small_maze7.cfg',
    '_track/track_obj_small_maze8.cfg',
    '_track/track_obj_small_maze9.cfg',
    '_track/track_obj_small_maze10.cfg',
    '_track/track_obj_small_maze11.cfg',
    '_track/track_obj_small_maze12.cfg',
    '_track/track_obj_small_maze13.cfg',
    '_track/track_obj_small_maze14.cfg',
    '_track/track_obj_small_maze15.cfg',
    '_track/track_obj_small_maze16.cfg',
    '_track/track_obj_small_maze17.cfg',
    '_track/track_obj_small_maze18.cfg',
    '_track/track_obj_small_maze19.cfg',
    '_track/track_obj_small_maze20.cfg',
    '_track/track_obj_small_maze21.cfg',
]
SKIP_FRAME = 1

logger = logging.getLogger(__name__)


class TrackObjSmallMazeEnvImpl(TrackObjV2Env):

    # init
    def __init__(self, game_cfg, has_gamevar=False, visible=False, is_spectator=False):
        if has_gamevar:
            raise Exception("TrackObjSmallMazeEnv_Impl has NO gamevar, must set has_gamevar=False")
        BaseEnv.__init__(self, game_cfg, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)


class TrackObjSmallMazeRandV2Env(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        self.has_gamevar = has_gamevar
        self.visible = visible
        self.is_spectator = is_spectator

        self.observation_space = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

        self._reset()

    # reset
    def _reset(self):
        self.h_impl = self._choose_rand_env()
        return self.h_impl._reset()

    def _choose_rand_env(self):
        game_cfg = choice(GAME_CFG_LIST)
        return TrackObjSmallMazeEnvImpl(game_cfg, self.has_gamevar, self.visible, self.is_spectator)

    # step
    def _step(self, action):
        return self.h_impl._step(action)

    # render
    def _render(self, mode='human', close=False):
        return self.h_impl._render(mode, close)
