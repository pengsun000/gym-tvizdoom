import logging
from random import choice

import gym
from gym import spaces

from base_env import BaseEnv
from base_env import SCREEN_SHAPE
from c_chamber_env import CChamberEnv
from c_chamber1_env import CChamber1Env
from c_chamber2_env import CChamber2Env
from c_chamber3_env import CChamber3Env
from c_chamber4_env import CChamber4Env
from c_chamber51_env import CChamber51Env

REAL_ACTIONS = [
    [True, False, False, False, False, False],
    [False, True, False, False, False, False],
    [False, False, True, False, False, False],
    [False, False, False, True, False, False],
    [False, False, False, False, True, False],
    [False, False, False, False, False, True],
]
ENV_LIST = [
    CChamberEnv,
    CChamber1Env,
    CChamber2Env,
    CChamber3Env,
    CChamber4Env,
    CChamber51Env
]
SKIP_FRAME = 1
GAMEVAR_SHAPE = (2,)

logger = logging.getLogger(__name__)


class CChamberRandEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        self.has_gamevar = has_gamevar
        self.visible = visible
        self.is_spectator = is_spectator

        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

        self._reset()

    # reset
    def _reset(self):
        self.h_impl = self._choose_rand_env()
        return self.h_impl._reset()

    def _choose_rand_env(self):
        env = choice(ENV_LIST)(self.has_gamevar, self.visible, self.is_spectator)
        return env

    # step
    def _step(self, action):
        return self.h_impl._step(action)

    # render
    def _render(self, mode='human', close=False):
        return self.h_impl._render(mode, close)
