import os.path as path

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from base_env import BaseEnv
from base_env import SCREEN_SHAPE

REAL_ACTIONS = [
    [True, False, False, False, False, False],
    [False, True, False, False, False, False],
    [False, False, True, False, False, False],
    [False, False, False, True, False, False],
    [False, False, False, False, True, False],
    [False, False, False, False, False, True],
]
GAME_CFG = '_combat/meetm.cfg'
SKIP_FRAME = 1
GAMEVAR_SHAPE = (2,)


logger = logging.getLogger(__name__)


class CMeetMEnv(BaseEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        super(CMeetMEnv, self).__init__(GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)

    def _init_observation_space(self):
        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    # reset
    def _reset_gamevar(self):
        self.gamevar = np.ndarray(GAMEVAR_SHAPE, dtype='float32')
        self.gamevar.fill(1.0)

    def _reset_reward(self):
        super(CMeetMEnv, self)._reset_reward()

    # step
    def _after_check_terminal(self):
        ind_fragcount, ind_health = 0, 1

        state = self.game.get_state()
        if state is None:
            return

        if float(state.game_variables[ind_health]) < 10.0:  # one death
            self.reward, self.terminal = -1.0, True
            return

        if self.reward > 0.0:  # earn one fragcount
            self.terminal = True

    def _update_gamevar(self, game_variables):
        ind_health, ind_ammor = 1, 2

        # health
        h = float(game_variables[ind_health]) / 100.0
        h = 0.0 if h < 0.0 else h  # full-health on birth
        self.gamevar[0] = h

        # amor
        self.gamevar[1] = float(game_variables[ind_ammor]) / 15.0

    def _update_reward(self, state):
        pass  # has been done in _after_check_terminal
