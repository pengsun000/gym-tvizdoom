import logging
from c_base_mpprot_greedy2rs_env import CBaseMpprotGreedy2rsEnv

GAME_CFG = '_combat/test3.cfg'
logger = logging.getLogger(__name__)


class CTest3ProtGreedyEnv(CBaseMpprotGreedy2rsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CTest3ProtGreedyEnv, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator,
                                                  skip_frame=4)
