import os.path as path
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from base_env import BaseEnv
from base_env import SCREEN_SHAPE

REAL_ACTIONS = [
    [True, True, True],
    [True, True, False],
    [True, False, True],
    [True, False, False],
    [False, True, True],
    [False, True, False],
    [False, False, True],
    [False, False, False],
]
GAME_CFG = '_internal/health_gather_twotypes.cfg'
SKIP_FRAME = 4
GAMEVAR_SHAPE = (1,)


logger = logging.getLogger(__name__)


class HealthGatherTwotypesEnv(BaseEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        super(HealthGatherTwotypesEnv, self).__init__(GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)

    def _init_observation_space(self):
        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    def __del__(self):
        self.game.close()

    # reset
    def _reset_gamevar(self):
        self.gamevar = np.ndarray(GAMEVAR_SHAPE, dtype='float32')
        self.gamevar.fill(1.0)

    # step
    def _update_gamevar(self, game_variables):
        self.gamevar[0] = float(game_variables[0]) / 100.0
