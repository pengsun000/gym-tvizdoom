import logging

from base_env import BaseEnv
from track_obj_shift_env import TrackObjShiftEnv


REAL_ACTIONS = [
    [False, False],
    [False, True],
    [True, False],
]
GAME_CFG = '_track/track_obj_shift_slow_close_small_maze.cfg'
SKIP_FRAME = 1

logger = logging.getLogger(__name__)


class TrackObjShiftSlowCloseSmallMazeEnv(TrackObjShiftEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        if has_gamevar:
            raise Exception("TrackObjEasyEnv has NO gamevar, must set has_gamevar=False")
        BaseEnv.__init__(self, GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)
