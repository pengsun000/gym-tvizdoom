# basic tasks
from gym_tvizdoom.envs.health_gather_env import HealthGatherEnv
from gym_tvizdoom.envs.health_gather_twotypes_env import HealthGatherTwotypesEnv


# combat
from gym_tvizdoom.envs.c_small_env import CSmallEnv
from gym_tvizdoom.envs.c_meet_env import CMeetEnv
from gym_tvizdoom.envs.c_meetm_env import CMeetMEnv

from gym_tvizdoom.envs.c_chamber_env import CChamberEnv
from gym_tvizdoom.envs.c_chamber1_env import CChamber1Env
from gym_tvizdoom.envs.c_chamber2_env import CChamber2Env
from gym_tvizdoom.envs.c_chamber3_env import CChamber3Env
from gym_tvizdoom.envs.c_chamber4_env import CChamber4Env
from gym_tvizdoom.envs.c_chamber5_env import CChamber5Env
from gym_tvizdoom.envs.c_chamber51_env import CChamber51Env
from gym_tvizdoom.envs.c_chamber_rand_env import CChamberRandEnv

from gym_tvizdoom.envs.c_flat_env import CFlatEnv
from gym_tvizdoom.envs.c_flat0_env import CFlat0Env
from gym_tvizdoom.envs.c_flat1_env import CFlat1Env
from gym_tvizdoom.envs.c_flat2_env import CFlat2Env
from gym_tvizdoom.envs.c_flat3_env import CFlat3Env
from gym_tvizdoom.envs.c_flat4_env import CFlat4Env
from gym_tvizdoom.envs.c_flat5_env import CFlat5Env
from gym_tvizdoom.envs.c_flat6_env import CFlat6Env
from gym_tvizdoom.envs.c_flat7_env import CFlat7Env
from gym_tvizdoom.envs.c_flat8_env import CFlat8Env
from gym_tvizdoom.envs.c_flat_rand_env import CFlatRandEnv

from gym_tvizdoom.envs.c_test_env import CTestEnv
from gym_tvizdoom.envs.c_test2_wu_env import CTest2WuEnv
from gym_tvizdoom.envs.c_test2_greedy_env import CTest2GreedyEnv
from gym_tvizdoom.envs.c_test2_prot_greedy_env import CTest2ProtGreedyEnv
from gym_tvizdoom.envs.c_test3_prot_greedy_env import CTest3ProtGreedyEnv

from gym_tvizdoom.envs.c_flatcig0_env import CFlatcig0Env
from gym_tvizdoom.envs.c_flatcig1_env import CFlatcig1Env
from gym_tvizdoom.envs.c_flatcig2_env import CFlatcig2Env
from gym_tvizdoom.envs.c_flatcig3_env import CFlatcig3Env
from gym_tvizdoom.envs.c_flatcig4_env import CFlatcig4Env
from gym_tvizdoom.envs.c_flatcig5_env import CFlatcig5Env
from gym_tvizdoom.envs.c_flatcig6_env import CFlatcig6Env
from gym_tvizdoom.envs.c_flatcig7_env import CFlatcig7Env
from gym_tvizdoom.envs.c_flatcig_rand_env import CFlatcigRandEnv

from gym_tvizdoom.envs.c_richcig0_env import CRichcig0Env
from gym_tvizdoom.envs.c_richcig1_env import CRichcig1Env
from gym_tvizdoom.envs.c_richcig2_env import CRichcig2Env
from gym_tvizdoom.envs.c_richcig3_env import CRichcig3Env
from gym_tvizdoom.envs.c_richcig4_env import CRichcig4Env
from gym_tvizdoom.envs.c_richcig5_env import CRichcig5Env
from gym_tvizdoom.envs.c_richcig6_env import CRichcig6Env
from gym_tvizdoom.envs.c_richcig7_env import CRichcig7Env
from gym_tvizdoom.envs.c_richcig_rand_env import CRichcigRandEnv

from gym_tvizdoom.envs.c_northcig0_env import CNorthcig0Env
from gym_tvizdoom.envs.c_westcig0_env import CWestcig0Env
from gym_tvizdoom.envs.c_eastcig0_env import CEastcig0Env
from gym_tvizdoom.envs.c_southcig0_env import CSouthcig0Env

from gym_tvizdoom.envs.c_cig_env import CCigEnv
from gym_tvizdoom.envs.c_cig_rand_env import CCigRandEnv
from gym_tvizdoom.envs.c_cig_rand2_env import CCigRand2Env

from gym_tvizdoom.envs.c_flatcig_attack0_env import CFlatcigAttack0Env

# tracking
from gym_tvizdoom.envs.track_obj_v2_env import TrackObjV2Env
# tracking, noise
from gym_tvizdoom.envs.track_obj_noise_v2_env import TrackObjNoiseV2Env
from gym_tvizdoom.envs.track_obj_noise2_v2_env import TrackObjNoise2V2Env
# tracking, background
from gym_tvizdoom.envs.track_obj_floorceil_v2_env import TrackObjFloorCeilV2Env
# tracking, other object
from gym_tvizdoom.envs.track_obj_zombie_v2_env import TrackObjZombieV2Env
from gym_tvizdoom.envs.track_obj_marine_v2_env import TrackObjMarineV2Env
from gym_tvizdoom.envs.track_obj_marine2_v2_env import TrackObjMarine2V2Env
from gym_tvizdoom.envs.track_obj_monsterx_v2_env import TrackObjMonsterxV2Env
# tracking, small maze
from gym_tvizdoom.envs.track_obj_small_maze1_v2_env import TrackObjSmallMaze1V2Env
from gym_tvizdoom.envs.track_obj_small_maze_rand_v2_env import TrackObjSmallMazeRandV2Env
from gym_tvizdoom.envs.track_obj_small_maze_rand_flip_v2_env import TrackObjSmallMazeRandFlipV2Env
# tracking o maze
from gym_tvizdoom.envs.track_obj_o_maze_env import TrackObjOMazeEnv
from gym_tvizdoom.envs.track_obj_o_maze_corner_env import TrackObjOMazeCornerEnv
from gym_tvizdoom.envs.track_obj_corridor_env import TrackObjCorridorEnv
# tracking, easy env
from gym_tvizdoom.envs.track_obj_easy_env import TrackObjEasyEnv
# tracking, hard env
from gym_tvizdoom.envs.track_obj_hard_v2_env import TrackObjHardV2Env
from gym_tvizdoom.envs.track_obj_back_v2_env import TrackObjBackV2Env
from gym_tvizdoom.envs.track_obj_back2_v2_env import TrackObjBack2V2Env
from gym_tvizdoom.envs.track_obj_counterclockwise_env import TrackObjCounterclockwiseEnv
# tracking, shift
from gym_tvizdoom.envs.track_obj_shift_env import TrackObjShiftEnv
from gym_tvizdoom.envs.track_obj_shift_slow_close_env import TrackObjShiftSlowCloseEnv
from gym_tvizdoom.envs.track_obj_shift_slow_close_small_maze_env import TrackObjShiftSlowCloseSmallMazeEnv
from gym_tvizdoom.envs.track_obj_shift_slow_close_small_maze_rtl_env import TrackObjShiftSlowCloseSmallMazeRTLEnv
from gym_tvizdoom.envs.track_obj_shift_slow_close_rd_env import TrackObjShiftSlowCloseRDEnv
# tracking, v3
from gym_tvizdoom.envs.track_obj_v3_env import TrackObjV3Env
