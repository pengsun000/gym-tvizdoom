import os.path as path
from math import sqrt, cos, sin, pi
from numpy import sign

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from base_env import BaseEnv
from base_env import SCREEN_SHAPE

TERMINATE_DIST_THRESHOLD = 128

REAL_ACTIONS = [
    [False, False],
    [False, True],
    [True, False],
]
GAME_CFG = '_track/track_obj_shift_slow_close.cfg'
SKIP_FRAME = 1

logger = logging.getLogger(__name__)


def world_to_local(x0, y0, a0, xt, yt, at):
    # vizdoom fixed point angle to radius
    theta = 2 * pi * a0
    # orientation to rotation
    theta -= pi / 2
    # common origin of world and local coordinate system
    dx, dy = xt - x0, yt - y0
    # coordinate rotation
    x_ = dx * cos(theta) + dy * sin(theta)
    y_ = -dx * sin(theta) + dy * cos(theta)
    a_ = a0 - at
    return x_, y_, a_


def local_coord_to_reward(x, x_last):
    reward = sign(x_last) * sign(x_last - x)
    return reward


class TrackObjShiftSlowCloseRDEnv(BaseEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        if has_gamevar:
            raise Exception("TrackObjEasyEnv has NO gamevar, must set has_gamevar=False")
        super(TrackObjShiftSlowCloseRDEnv, self).__init__(GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible,
                                                          is_spectator)

    def _init_observation_space(self):
        self.observation_space = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    # reset
    def _reset_reward(self):
        super(TrackObjShiftSlowCloseRDEnv, self)._reset_reward()
        self.episode_reward = 0
        # reset last and current object x coord
        x_, y_, a_ = self._compute_current_local_coord()
        self.xlocal = self.xlocal_last = x_

    # step
    def _after_make_action(self):
        # updating local coord
        self.xlocal_last = self.xlocal
        x_, y_, a_ = self._compute_current_local_coord()
        self.xlocal = x_

    def _after_check_terminal(self):
        dist = abs(self.xlocal)
        if dist > TERMINATE_DIST_THRESHOLD:
            self.terminal = True

    def _update_reward(self, state):
        self.reward = local_coord_to_reward(self.xlocal, self.xlocal_last)
        self.episode_reward += self.reward

    def _compute_current_local_coord(self):
        def cvt(var):
            tmp = self.game.get_game_variable(var)  # Get value of scripted variable
            return doom_fixed_to_double(tmp)

        # agent's coordinate
        x = cvt(GameVariable.USER1)
        y = cvt(GameVariable.USER2)
        a = cvt(GameVariable.USER3)
        # object's coordinate
        xx = cvt(GameVariable.USER4)
        yy = cvt(GameVariable.USER5)
        aa = cvt(GameVariable.USER6)

        x_, y_, a_ = world_to_local(x, y, a, xx, yy, aa)
        return x_, y_, a_