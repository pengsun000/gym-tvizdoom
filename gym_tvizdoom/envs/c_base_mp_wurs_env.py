""" Wu Yuxin's reward shaping, multiplayer """
import logging
import copy
import numpy as np

from c_base_wurs_env import CBaseWursENV
from c_base_wurs_env import SCREEN_SHAPE
from c_base_wurs_env import REAL_ACTIONS
from c_base_wurs_env import GAMEVAR_SHAPE
from c_base_wurs_env import LIFE_REAL_START

NUM_BOTS = 8
IND_GAMEVAR_FRAG = 0

logger = logging.getLogger(__name__)


class CBaseMpWursEnv(CBaseWursENV):

    # init
    def __init__(self, game_cfg, has_rwdshape=True, has_gamevar=False, visible=False, is_spectator=False, skip_frame=1):
        super(CBaseMpWursEnv, self).__init__(game_cfg, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame)

    def _before_init_game(self):
        super(CBaseMpWursEnv, self)._before_init_game()
        self.game.add_game_args("-host 1 -deathmatch "
                           "+sv_forcerespawn 1 +sv_noautoaim 1 +sv_respawnprotect 0 +sv_spawnfarthest 1")
        self.game.add_game_args("+name WhoAmI +colorset 0")

    # reset
    def _after_new_episode(self):
        self.game.send_game_command("removebots")
        for i in range(0, NUM_BOTS):
            self.game.send_game_command("addbot")
        self.num_step = 0
        #raise NotImplementedError

    def _reset_gamevar(self):
        super(CBaseMpWursEnv, self)._reset_gamevar()
        self.frag = None

    # step
    def _update_gamevar(self, game_variables):
        super(CBaseMpWursEnv, self)._update_gamevar(game_variables)
        self.frag_prev = copy.deepcopy(self.frag)
        self.frag = float(game_variables[IND_GAMEVAR_FRAG])

    def _update_reward(self, state):
        self.reward = 0  # don't use internal reward
        super(CBaseMpWursEnv, self)._update_reward(state)
        self._update_frag_reward(state)

    def _update_frag_reward(self, state):
        if self.frag_prev is not None and self.step_this_life > LIFE_REAL_START:
            r = self.frag - self.frag_prev
            self.reward += np.sign(r)
