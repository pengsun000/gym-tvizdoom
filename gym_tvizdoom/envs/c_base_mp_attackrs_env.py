""" attack reward shaping, multiplayer """
import logging
import copy
import numpy as np

from c_base_attackrs_env import CBaseAttackrsEnv
from c_base_attackrs_env import SCREEN_SHAPE
from c_base_attackrs_env import REAL_ACTIONS
from c_base_attackrs_env import GAMEVAR_SHAPE
from c_base_attackrs_env import LIFE_REAL_START

NUM_BOTS = 8

logger = logging.getLogger(__name__)


class CBaseMpAttackrsEnv(CBaseAttackrsEnv):

    # init
    def __init__(self, game_cfg, has_rwdshape=True, has_gamevar=False, visible=False, is_spectator=False):
        super(CBaseMpAttackrsEnv, self).__init__(game_cfg, has_rwdshape, has_gamevar, visible, is_spectator)

    def _before_init_game(self):
        super(CBaseMpAttackrsEnv, self)._before_init_game()
        self.game.add_game_args("-host 1 -deathmatch "
                           "+sv_forcerespawn 1 +sv_noautoaim 1 +sv_respawnprotect 0 +sv_spawnfarthest 1")
        self.game.add_game_args("+name WhoAmI +colorset 0")

    # reset
    def _after_new_episode(self):
        self.game.send_game_command("removebots")
        for i in range(0, NUM_BOTS):
            self.game.send_game_command("addbot")
        self.num_step = 0
        #raise NotImplementedError

    # step
    def _update_reward(self, state):
        self.reward = 0  # don't use internal reward
        super(CBaseMpAttackrsEnv, self)._update_reward(state)
