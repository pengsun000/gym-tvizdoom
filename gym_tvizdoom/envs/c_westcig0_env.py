import logging
from c_base_mpprot_greedy2rs_env import CBaseMpprotGreedy2rsEnv

GAME_CFG = '_combat/westcig0.cfg'
logger = logging.getLogger(__name__)


class CWestcig0Env(CBaseMpprotGreedy2rsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CWestcig0Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame=4,
                                           num_bots=15)
