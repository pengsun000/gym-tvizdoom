import logging
from c_base_mp_attackrs_env import CBaseMpAttackrsEnv

GAME_CFG = '_combat/flatcig_attack0.cfg'
logger = logging.getLogger(__name__)


class CFlatcigAttack0Env(CBaseMpAttackrsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CFlatcigAttack0Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator)
