import logging
from random import choice

import gym
from gym import spaces

from c_base_greedy2rs_env import SCREEN_SHAPE
from c_base_greedy2rs_env import REAL_ACTIONS
from c_base_greedy2rs_env import GAMEVAR_SHAPE

from c_flatcig0_env import CFlatcig0Env
from c_flatcig1_env import CFlatcig1Env
from c_flatcig2_env import CFlatcig2Env
from c_flatcig3_env import CFlatcig3Env
from c_flatcig4_env import CFlatcig4Env
from c_flatcig5_env import CFlatcig5Env
from c_flatcig6_env import CFlatcig6Env
from c_flatcig7_env import CFlatcig7Env


ENV_LIST = [
    CFlatcig0Env,
    CFlatcig1Env,
    CFlatcig2Env,
    CFlatcig3Env,
    CFlatcig4Env,
    CFlatcig5Env,
    CFlatcig6Env,
    CFlatcig7Env,
]

logger = logging.getLogger(__name__)


class CFlatcigRandEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        self.has_rwdshape = has_rwdshape
        self.has_gamevar = has_gamevar
        self.visible = visible
        self.is_spectator = is_spectator

        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

        self._reset()

    # reset
    def _reset(self):
        self.h_impl = self._choose_rand_env()
        return self.h_impl._reset()

    def _choose_rand_env(self):
        env = choice(ENV_LIST)(self.has_rwdshape, self.has_gamevar, self.visible, self.is_spectator)
        return env

    # step
    def _step(self, action):
        return self.h_impl._step(action)

    # render
    def _render(self, mode='human', close=False):
        return self.h_impl._render(mode, close)
