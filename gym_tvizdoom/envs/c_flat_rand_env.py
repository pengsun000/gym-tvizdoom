import logging
from random import choice

import gym
from gym import spaces

from c_base_wurs_env import SCREEN_SHAPE
from c_base_wurs_env import REAL_ACTIONS
from c_base_wurs_env import GAMEVAR_SHAPE

from c_flat1_env import CFlat1Env
from c_flat2_env import CFlat2Env
from c_flat3_env import CFlat3Env
from c_flat4_env import CFlat4Env
from c_flat5_env import CFlat5Env
from c_flat6_env import CFlat6Env
from c_flat7_env import CFlat7Env
from c_flat8_env import CFlat8Env

ENV_LIST = [
    CFlat1Env,
    CFlat2Env,
    CFlat3Env,
    CFlat4Env,
    CFlat5Env,
    CFlat6Env,
    CFlat7Env,
    CFlat8Env,
]

logger = logging.getLogger(__name__)


class CFlatRandEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        self.has_rwdshape = has_rwdshape
        self.has_gamevar = has_gamevar
        self.visible = visible
        self.is_spectator = is_spectator

        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

        self._reset()

    # reset
    def _reset(self):
        self.h_impl = self._choose_rand_env()
        return self.h_impl._reset()

    def _choose_rand_env(self):
        env = choice(ENV_LIST)(self.has_rwdshape, self.has_gamevar, self.visible, self.is_spectator)
        return env

    # step
    def _step(self, action):
        return self.h_impl._step(action)

    # render
    def _render(self, mode='human', close=False):
        return self.h_impl._render(mode, close)
