import os.path as path

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging


SCREEN_SHAPE = (120, 160, 3)  # (H, W, C)
SCREEN_RESOLUTION = ScreenResolution.RES_160X120


logger = logging.getLogger(__name__)


class BaseEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    # init
    def __init__(self, game_cfg, real_actions, skip_frame, has_gamevar=False, visible=False, is_spectator=False):
        self.game_cfg = game_cfg
        self.real_actions = real_actions
        self.skip_frame = skip_frame
        self.has_gamevar = has_gamevar
        self.visible = visible
        self.is_spectator = is_spectator

        self.game = DoomGame()
        self._before_init_game()
        self.game.init()

        self._init_observation_space()
        self._init_action_space()

        self._reset()

        logger.setLevel(logging.INFO)
        logger.info("vizdoom environment {} initialized".format(game_cfg))

        # __init_observation_space & __init_action_space in derived class!

        return

    def _before_init_game(self):
        config_path = path.join(path.dirname(__file__), self.game_cfg)
        self.game.load_config(config_path)
        self.game.set_screen_resolution(SCREEN_RESOLUTION)
        self.game.set_window_visible(self.visible)
        mode = Mode.SPECTATOR if self.is_spectator else Mode.PLAYER
        self.game.set_mode(mode)  # player, spectator, etc

    def _init_observation_space(self):
        raise NotImplementedError

    def _init_action_space(self):
        raise NotImplementedError

    def __del__(self):
        self.game.close()

    # reset
    def _reset(self):
        """ reset episode """
        self.game.new_episode()
        self._after_new_episode()

        # enforce an initial state
        self._reset_screen()
        self._reset_gamevar()
        self._reset_reward()
        self.terminal = False

        if self.has_gamevar:
            return self.s_t, self.gamevar
        return self.s_t

    def _after_new_episode(self):
        pass

    def _reset_screen(self):
        self.s_t = np.ndarray(SCREEN_SHAPE, dtype='uint8')
        self.s_t.fill(0)

    def _reset_gamevar(self):
        self.gamevar = None

    def _reset_reward(self):
        self.reward = 0

    # step
    def _step(self, action):
        # take action & get current reward
        self._before_make_action()
        self.reward = self.game.make_action(self.real_actions[action], self.skip_frame)
        self._after_make_action()

        # set current screen, game_varaible
        state = self.game.get_state()
        if state is not None:
            self._update_screen(state.screen_buffer)
            if self.has_gamevar:
                self._update_gamevar(state.game_variables)
            self._update_reward(state)

        # terminal ?
        self.terminal = self.game.is_episode_finished()
        self._after_check_terminal()

        if self.has_gamevar:
            return (self.s_t, self.gamevar), self.reward, self.terminal, {}
        return self.s_t, self.reward, self.terminal, {}

    def _before_make_action(self):
        pass

    def _after_make_action(self):
        pass

    def _after_check_terminal(self):
        pass

    def _update_screen(self, screen):
        # permute dimension (C, H, W) - > (H, W, C)
        self.s_t = np.transpose(screen, axes=(1, 2, 0))

    def _update_gamevar(self, game_variables):
        pass

    def _update_reward(self, state):
        pass

    # render
    def _render(self, mode='human', close=False):
        if mode == 'rgb_array':
            return self.s_t
        pass
