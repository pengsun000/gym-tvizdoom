""" Greedy2 reward shaping, multiplayer """
import logging
import copy
import numpy as np

from c_base_greedy2rs_env import CBaseGreedy2rsEnv
from c_base_greedy2rs_env import LIFE_REAL_START

NUM_BOTS = 6
IND_GAMEVAR_FRAG = 0

logger = logging.getLogger(__name__)


class CBaseMpGreedy2rsEnv(CBaseGreedy2rsEnv):

    # init
    def __init__(self, game_cfg, has_rwdshape=True, has_gamevar=False, visible=False, is_spectator=False, skip_frame=1):
        super(CBaseMpGreedy2rsEnv, self).__init__(game_cfg, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame)

    def _before_init_game(self):
        super(CBaseMpGreedy2rsEnv, self)._before_init_game()
        self.game.add_game_args("-host 1 -deathmatch "
                           "+sv_forcerespawn 1 +sv_noautoaim 1 +sv_respawnprotect 0 +sv_spawnfarthest 1")
        self.game.add_game_args("+name WhoAmI +colorset 0")

    # reset
    def _after_new_episode(self):
        self.game.send_game_command("removebots")
        for i in range(0, NUM_BOTS):
            self.game.send_game_command("addbot")
        self.num_step = 0
        #raise NotImplementedError

    def _reset_gamevar(self):
        super(CBaseMpGreedy2rsEnv, self)._reset_gamevar()
        self.frag = None

    # step
    def _update_reward(self, state):
        self.reward = 0  # don't use internal reward
        super(CBaseMpGreedy2rsEnv, self)._update_reward(state)

    def _update_frag_reward(self, state):
        if self.frag_prev is not None and self.step_this_life > LIFE_REAL_START:
            r = self.frag - self.frag_prev
            self.reward += np.sign(r)
