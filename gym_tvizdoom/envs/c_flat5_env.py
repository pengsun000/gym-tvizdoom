import logging
from c_base_greedy2rs_env import CBaseGreedy2rsEnv

GAME_CFG = '_combat/flat5.cfg'
logger = logging.getLogger(__name__)


class CFlat5Env(CBaseGreedy2rsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CFlat5Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator)
