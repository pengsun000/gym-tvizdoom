import logging

from base_env import BaseEnv
from track_obj_v2_env import TrackObjV2Env


REAL_ACTIONS = [
    [True, False, False],
    [False, True, False],
    [True, False, True],
    [False, True, True],
    [False, False, True],
    [False, False, False],
]
GAME_CFG = '_track/track_obj_o_maze_corner.cfg'
SKIP_FRAME = 1

logger = logging.getLogger(__name__)


class TrackObjOMazeCornerEnv(TrackObjV2Env):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        if has_gamevar:
            raise Exception("TrackObjOMazeCornerEnv has NO gamevar, must set has_gamevar=False")
        BaseEnv.__init__(self, GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)
