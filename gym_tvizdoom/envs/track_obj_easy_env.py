import os.path as path
from math import sqrt, cos, sin, pi

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging


OBJ_EXPECTED_DIST = 128
OBJ_EXPECTED_RADIUS = 32
OBJ_PERFECT_REWARD = 1.0
NUM_INIT_RAND_ACTIONS = 8
TERMINATE_REWARD_THRESHOLD = -10

REAL_ACTIONS = [
    [True, False, False],
    [False, True, False],
    [True, False, True],
    [False, True, True],
    [False, False, True],
    [False, False, False],
]

ACTION_SIZE = len(REAL_ACTIONS)

GAME_CFG = '_track/track_obj_easy.cfg'
SKIP_FRAME = 1
MODE = Mode.PLAYER
# MODE = Mode.SPECTATOR
SCREEN_SHAPE = (120, 160, 3)
SCREEN_RESOLUTION = ScreenResolution.RES_160X120


def coord_to_reward(x, y, a, xx, yy, aa):
    """Convert coordinate difference to reward

    (x, y, a) is agent's coordinate & orientation angle
    (xx, yy, aa) is object's coordinate"""
    def world_to_local(x0, y0, a0, xt, yt, at):
        # vizdoom fixed point angle to radius
        theta = 2 * pi * a0
        # orientation to rotation
        theta -= pi/2
        # common origin of world and local coordinate system
        dx, dy = xt - x0, yt - y0
        # coordinate rotation
        x_ = dx * cos(theta) + dy * sin(theta)
        y_ = -dx * sin(theta) + dy * cos(theta)
        a_ = a0 - at
        return x_, y_, a_

    def comp_dist_angle_reward(x, y, a_diff, x_offset=0, y_offset=0):
        dx = x - x_offset
        dy = y - y_offset
        dist = sqrt(dx * dx + dy * dy)
        reward_dist = 1.0 if dist <= OBJ_EXPECTED_DIST else -1.0

        aa_diff = abs(float(a_diff))  # in [0, 1]
        angle = min(aa_diff, 1.0 - aa_diff)
        reward_angle = 1.0 - 4 * angle

        reward = 0.5*reward_dist + 0.0*reward_angle
        return reward

    xx_, yy_, aa_ = world_to_local(x, y, a, xx, yy, aa)  # object in agent's coordinate

    reward = comp_dist_angle_reward(xx_, yy_, aa_, x_offset=0, y_offset=OBJ_EXPECTED_DIST)

    return reward

logger = logging.getLogger(__name__)


class TrackObjEasyEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self, visible=False):
        self.game = DoomGame()
        config_path = path.join(path.dirname(__file__), GAME_CFG)
        self.game.load_config(config_path)
        self.game.set_screen_resolution(SCREEN_RESOLUTION)
        self.game.set_window_visible(visible)
        self.game.set_mode(MODE)  # player, spectator, etc
        self.game.init()

        self.observation_space = spaces.Box(low=0.0, high=255, shape=SCREEN_SHAPE)

        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

        self.reward, self.terminal, self.s_t = None, None, None
        self.episode_reward = None
        self._reset()

        logger.setLevel(logging.INFO)
        logger.info("vizdoom environment {} initialized".format(GAME_CFG))

    def __del__(self):
        self.game.close()

    def _step(self, action):
        # take action
        self.reward = self.game.make_action(REAL_ACTIONS[action], SKIP_FRAME)

        # reward
        self.reward = self._process_reward()
        self.episode_reward += self.reward

        # terminal ?
        self.terminal = self.game.is_episode_finished()
        if self.episode_reward <= TERMINATE_REWARD_THRESHOLD:
            self.terminal = True

        # set current screen
        state = self.game.get_state()
        if state is not None:
            self.s_t = self._process_screen(state.screen_buffer)
        else:
            self.s_t = self._null_screen()

        return self.s_t, self.reward, self.terminal, {}

    def _reset(self):
        """ reset episode """
        self.game.new_episode()
        self.episode_reward = 0

        # enforce an initial state
        self.s_t = self._null_screen()
        self.reward, self.episode_reward = 0, 0
        self.terminal = False

        return self.s_t

    def _render(self, mode='human', close=False):
        if mode == 'rgb_array':
            return self.s_t
        pass

    def _null_screen(self):
        x = np.ndarray(SCREEN_SHAPE, dtype='uint8')
        x.fill(0)
        return x

    def _process_screen(self, screen):
        # permute dimension (C, H, W) - > (H, W, C)
        x = np.transpose(screen, axes=(1, 2, 0))
        return x

    def _process_reward(self):
        def cvt(var):
            tmp = self.game.get_game_variable(var)  # Get value of scripted variable
            return doom_fixed_to_double(tmp)

        # agent's coordinate
        x = cvt(GameVariable.USER1)
        y = cvt(GameVariable.USER2)
        a = cvt(GameVariable.USER3)
        # object's coordinate
        xx = cvt(GameVariable.USER4)
        yy = cvt(GameVariable.USER5)
        aa = cvt(GameVariable.USER6)  # useless...

        reward = coord_to_reward(x, y, a, xx, yy, aa)

        return reward
