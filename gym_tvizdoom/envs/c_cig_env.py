import logging
from c_base_mpprot_greedy2rs_env import CBaseMpprotGreedy2rsEnv

GAME_CFG = '_combat/cig.cfg'
logger = logging.getLogger(__name__)


class CCigEnv(CBaseMpprotGreedy2rsEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        super(CCigEnv, self).__init__(GAME_CFG, has_rwdshape=False, has_gamevar=has_gamevar, visible=visible,
                                      is_spectator=is_spectator, skip_frame=4, num_bots=16)
