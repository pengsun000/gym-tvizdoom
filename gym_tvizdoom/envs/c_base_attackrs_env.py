""" Attack reward shaping """
import os.path as path
import copy
from math import sqrt

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from base_env import BaseEnv
from base_env import SCREEN_SHAPE

REAL_ACTIONS = [
    [True, False, False, False, False, False],
    [False, True, False, False, False, False],
    [False, False, True, False, False, False],
    [False, False, False, True, False, False],
    [False, False, False, False, True, False],
    [False, False, False, False, False, True],
]
SKIP_FRAME = 1
GAMEVAR_SHAPE = (2,)
LIFE_REAL_START = 3

IND_GAMEVAR_FRAG = 0
IND_GAMEVAR_HEALTH, IND_GAMEVAR_AMMOR = 1, 2
IND_HEALTH, IND_AMMOR = 0, 1


logger = logging.getLogger(__name__)


class CBaseAttackrsEnv(BaseEnv):

    # init
    def __init__(self, game_cfg, has_rwdshape=True, has_gamevar=False, visible=False, is_spectator=False):
        if not has_gamevar:
            raise Exception("CBaseWursENV: has_gamevar must = True")
        super(CBaseAttackrsEnv, self).__init__(game_cfg, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)

        self.has_rwdshape = has_rwdshape

    def _init_observation_space(self):
        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    # reset
    def _reset_gamevar(self):
        # primary: frag
        self.frag = None
        # configuration file game vars
        self.gamevar = np.ndarray(GAMEVAR_SHAPE, dtype='float32')
        self.gamevar.fill(1.0)
        # user data game vars
        self.pos = None
        # other book-keeping
        self.step_this_life = 0

    # step
    def _before_make_action(self):
        if self.game.is_player_dead():
            self._reset_gamevar()

    def _update_gamevar(self, game_variables):
        self._update_frag_gamevar(game_variables)
        self._update_cfg_gamevar(game_variables)
        self._update_usrdata_gamevar()
        self.step_this_life += 1

    def _update_reward(self, state):
        self._update_frag_reward(state)
        if self.has_rwdshape:
            self._update_living_reward(state)
            self._update_dist_reward(state)
            self._update_health_reward(state)
            self._update_ammor_reward(state)

    def _after_check_terminal(self):
        if self.step_this_life > LIFE_REAL_START:
            if self.frag > self.frag_prev:
                self.terminal = True

    # step helpers: update gamevar
    def _update_frag_gamevar(self, game_variables):
        self.frag_prev = copy.deepcopy(self.frag)
        self.frag = float(game_variables[IND_GAMEVAR_FRAG])

    def _update_cfg_gamevar(self, game_variables):
        # backup previous
        self.gamevar_prev = copy.deepcopy(self.gamevar)

        # update current
        # health
        h = float(game_variables[IND_GAMEVAR_HEALTH]) / 100.0
        h = 0.0 if h < 0.0 else h  # full-health on birth
        self.gamevar[0] = h

        # ammor
        self.gamevar[1] = float(game_variables[IND_GAMEVAR_AMMOR]) / 15.0

    def _update_usrdata_gamevar(self):
        def _get_agent_coord():
            def cvt(var):
                tmp = self.game.get_game_variable(var)  # Get value of scripted variable
                return doom_fixed_to_double(tmp)

            # agent's coordinate
            x = cvt(GameVariable.USER1)
            y = cvt(GameVariable.USER2)
            a = cvt(GameVariable.USER3)
            return x, y, a

        # backup previous
        self.pos_prev = copy.deepcopy(self.pos)

        # get current
        self.pos = _get_agent_coord()

    # step helpers: update reward
    def _update_frag_reward(self, state):
        if self.frag_prev is not None and self.step_this_life > LIFE_REAL_START:
            r = self.frag - self.frag_prev
            self.reward += 2.4*np.sign(r)

    def _update_living_reward(self, state):
        self.reward += -0.008

    def _update_dist_reward(self, state):
        pass

    def _update_health_reward(self, state):
        pass

    def _update_ammor_reward(self, state):
        r = self.gamevar[IND_AMMOR] - self.gamevar_prev[IND_AMMOR]
        if r != 0:
            self.reward += 1.0 if r > 0 else -0.1
