import os.path as path

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from base_env import BaseEnv
from base_env import SCREEN_SHAPE

REAL_ACTIONS = [
    [True, False, False, False, False, False],
    [False, True, False, False, False, False],
    [False, False, True, False, False, False],
    [False, False, False, True, False, False],
    [False, False, False, False, True, False],
    [False, False, False, False, False, True],
]
GAME_CFG = '_combat/meet.cfg'
SKIP_FRAME = 1
GAMEVAR_SHAPE = (2,)


logger = logging.getLogger(__name__)


class CMeetEnv(BaseEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        super(CMeetEnv, self).__init__(GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)

    def _before_init_game(self):
        super(CMeetEnv, self)._before_init_game()
        self.game.add_game_args("-host 1 -deathmatch "
                           "+sv_forcerespawn 1 +sv_noautoaim 1 +sv_respawnprotect 1 +sv_spawnfarthest 1")
        self.game.add_game_args("+name AI +colorset 0")

    def _init_observation_space(self):
        screen_sp = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)
        gamevar_sp = spaces.Box(low=0.0, high=1.0, shape=GAMEVAR_SHAPE)
        self.observation_space = spaces.Tuple([screen_sp, gamevar_sp]) if self.has_gamevar else screen_sp

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    # reset
    def _after_new_episode(self):
        self.game.send_game_command("removebots")
        self.game.send_game_command("addbot Plissken")
        self.num_step = 0

    def _reset_gamevar(self):
        self.gamevar = np.ndarray(GAMEVAR_SHAPE, dtype='float32')
        self.gamevar.fill(1.0)

    def _reset_reward(self):
        super(CMeetEnv, self)._reset_reward()

    # step
    def _before_make_action(self):
        pass

    def _after_make_action(self):
        self.num_step += 1
        if self.num_step <= 3 and self.game.is_player_dead():
            self.game.respawn_player()

    def _after_check_terminal(self):
        ind_fragcount, ind_health = 0, 1

        state = self.game.get_state()
        if state is None:
            return

        if float(state.game_variables[ind_health]) < 7.0:  # one death
            self.reward, self.terminal = -1.0, True
            return
        if float(state.game_variables[ind_fragcount]) > 0:  # earn one frag
            self.reward, self.terminal = +1.0, True
            return

    def _update_gamevar(self, game_variables):
        ind_health, ind_ammor = 1, 2

        # health
        h = float(game_variables[ind_health]) / 100.0
        h = 0.0 if h < 0.0 else h  # full-health on birth
        self.gamevar[0] = h

        # amor
        self.gamevar[1] = float(game_variables[ind_ammor]) / 15.0

    def _update_reward(self, state):
        pass  # has been done in _after_check_terminal
