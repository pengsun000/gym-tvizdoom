import logging
from c_base_mp_greedy2rs_env import CBaseMpGreedy2rsEnv

GAME_CFG = '_combat/test2.cfg'
logger = logging.getLogger(__name__)


class CTest2GreedyEnv(CBaseMpGreedy2rsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CTest2GreedyEnv, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame=4)
