import logging
from c_base_greedyrs_env import CBaseGreedyrsEnv

GAME_CFG = '_combat/flat6.cfg'
logger = logging.getLogger(__name__)


class CFlat6Env(CBaseGreedyrsEnv):

    # init
    def __init__(self, has_rwdshape=False, has_gamevar=False, visible=False, is_spectator=False):
        super(CFlat6Env, self).__init__(GAME_CFG, has_rwdshape, has_gamevar, visible, is_spectator, skip_frame=4)
