import os.path as path
from math import sqrt, cos, sin, pi

import gym
from gym import error, spaces, utils
from gym.utils import seeding

from vizdoom import DoomGame, ScreenResolution, GameVariable, Mode, doom_fixed_to_double
import numpy as np
import logging

from gym_tvizdoom.envs.base_env import BaseEnv
from gym_tvizdoom.envs.base_env import SCREEN_SHAPE

OBJ_EXPECTED_DIST = 128
OBJ_PERFECT_REWARD = 1.0
NUM_INIT_RAND_ACTIONS = 8
TERMINATE_REWARD_THRESHOLD = -450
TRUNC_DIST = 32

REAL_ACTIONS = [
    [True, False, False],
    [False, True, False],
    [True, False, True],
    [False, True, True],
    [False, False, True],
    [False, False, False],
]
GAME_CFG = '_track/track_obj.cfg'
SKIP_FRAME = 1

logger = logging.getLogger(__name__)


def coord_to_reward(x, y, a, xx, yy, aa):
    """Convert coordinate difference to reward

    (x, y, a) is agent's coordinate & orientation angle
    (xx, yy, aa) is object's coordinate"""
    def world_to_local(x0, y0, a0, xt, yt, at):
        # vizdoom fixed point angle to radius
        theta = 2 * pi * a0
        # orientation to rotation
        theta -= pi/2
        # common origin of world and local coordinate system
        dx, dy = xt - x0, yt - y0
        # coordinate rotation
        x_ = dx * cos(theta) + dy * sin(theta)
        y_ = -dx * sin(theta) + dy * cos(theta)
        a_ = a0 - at
        return x_, y_, a_

    def comp_parabolic_reward(x, y, x_offset=0, y_offset=0, a_diff=0):
        dx = x - x_offset
        dy = y - y_offset
        dist = abs(sqrt(dx * dx + dy * dy)-TRUNC_DIST)
        penalty_dist = dist/OBJ_EXPECTED_DIST
        penalty_angle = abs(a_diff)/pi
        penalty = penalty_dist + penalty_angle
        return OBJ_PERFECT_REWARD - penalty

    xx_, yy_, aa_ = world_to_local(x, y, a, xx, yy, aa)  # object in agent's coordinate

    reward = comp_parabolic_reward(xx_, yy_, x_offset=0, y_offset=OBJ_EXPECTED_DIST, a_diff=aa_)

    return reward


class TrackObjV2Env(BaseEnv):

    # init
    def __init__(self, has_gamevar=False, visible=False, is_spectator=False):
        if has_gamevar:
            raise Exception("TrackObjV2Env has NO gamevar, must set has_gamevar=False")
        super(TrackObjV2Env, self).__init__(GAME_CFG, REAL_ACTIONS, SKIP_FRAME, has_gamevar, visible, is_spectator)

    def _init_observation_space(self):
        self.observation_space = spaces.Box(low=0, high=255, shape=SCREEN_SHAPE)

    def _init_action_space(self):
        self.action_space = spaces.Discrete(len(REAL_ACTIONS))

    # reset
    def _reset_reward(self):
        super(TrackObjV2Env, self)._reset_reward()
        self.episode_reward = 0

    # step
    def _after_check_terminal(self):
        if self.episode_reward <= TERMINATE_REWARD_THRESHOLD:
            self.terminal = True

    def _update_reward(self, state):
        def cvt(var):
            tmp = self.game.get_game_variable(var)  # Get value of scripted variable
            return doom_fixed_to_double(tmp)

        # agent's coordinate
        x = cvt(GameVariable.USER1)
        y = cvt(GameVariable.USER2)
        a = cvt(GameVariable.USER3)
        # object's coordinate
        xx = cvt(GameVariable.USER4)
        yy = cvt(GameVariable.USER5)
        aa = cvt(GameVariable.USER6)

        self.reward = coord_to_reward(x, y, a, xx, yy, aa)
        self.episode_reward += self.reward
