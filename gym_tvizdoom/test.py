import gym
import gym.spaces as spaces
import gym_tvizdoom
import time
import random


max_episode = 20
sleep_time = 0.01
verbose = 0


def rand_agent_loop(env, max_episode, sleep_time):
    n_episode = 0
    it = 0
    episode_reward = 0
    env.reset()
    while n_episode < max_episode:
        action = random.randint(0, env.action_space.n - 1)

        state, reward, terminal, info = env.step(action)
        it += 1
        episode_reward += reward

        if verbose >= 3:
            print("it = {}, action = {}, reward = {}, episode_reward = {}".format(it, action, reward, episode_reward))
            if isinstance(env.observation_space, spaces.Tuple):
                print('additional states = ', state[1:])
        #if verbose >= 2 and reward < -0.01 or reward > 0.01:
        if verbose >= 2:
            print("it = {}, action = {}, reward = {}, episode_reward = {}".format(it, action, reward, episode_reward))
        time.sleep(sleep_time)

        if terminal:
            if verbose >= 1:
                print("Episode {} done".format(n_episode))
                print('==================')
            state = env.reset()
            it = 0
            n_episode += 1
            episode_reward = 0

    if verbose >= 1:
        print('done running {} episodes'.format(n_episode))
    return


#################
# basic
#################
#env = gym.make('HealthGather-v0')
#env = gym.make('HealthGatherVisible-v0')
#env = gym.make('HealthGatherGamevarVisible-v0')
#env = gym.make('HealthGatherGamevar-v0')
#env = gym.make('HealthGatherGamevarVisibleSpectator-v0')
#env = gym.make('HealthGatherGamevarSpectator-v0')
# env = gym.make('HealthGatherTwotypesGamevarVisibleSpectator-v0')


##################
# combat
##################
#env = gym.make('CSmallVisible-v0')
#env = gym.make('CSmallGamevarVisible-v0')
#env = gym.make('CSmallVisibleSpectator-v0')
# env = gym.make('CSmallGamevarVisibleSpectator-v0')

# env = gym.make('CMeetGamevarVisible-v0')
# env = gym.make('CMeetGamevarVisibleSpectator-v0')
# env = gym.make('CMeetMGamevarVisibleSpectator-v0')
# env = gym.make('CMeetMGamevarVisible-v0')

# env = gym.make('CChamberGamevarVisibleSpectator-v0')
# env = gym.make('CChamber1GamevarVisibleSpectator-v0')
# env = gym.make('CChamber2GamevarVisibleSpectator-v0')
# env = gym.make('CChamber3GamevarVisibleSpectator-v0')
# env = gym.make('CChamber4GamevarVisibleSpectator-v0')
# env = gym.make('CChamber5GamevarVisibleSpectator-v0')
# env = gym.make('CChamber51GamevarVisibleSpectator-v0')

# env = gym.make('CFlat0GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlatGamevarVisibleSpectator-v0')
# env = gym.make('CFlat1GamevarVisibleSpectator-v0')
# env = gym.make('CFlatGamevarRwdshapeVisible-v0')
# env = gym.make('CFlat2GamevarRwdshapeVisible-v0')
# env = gym.make('CFlat3GamevarRwdshapeVisible-v0')
# env = gym.make('CFlat3GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlat4GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlat5GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlat6GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlat7GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlat8GamevarRwdshapeVisibleSpectator-v0')

# env = gym.make('CFlatcig0GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlatcig1GamevarVisibleSpectator-v0')
# env = gym.make('CFlatcig1GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlatcig2GamevarVisibleSpectator-v0')
# env = gym.make('CFlatcig3GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlatcig4GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CFlatcig7GamevarRwdshapeVisibleSpectator-v0')

# env = gym.make('CRichcig0GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CRichcig7GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CRichcigRandGamevarRwdshapeVisible-v0')

# env = gym.make('CNorthcig0GamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CWestcig0GamevarRwdshapeVisibleSpectator-v0')

# env = gym.make('CChamberRandGamevarVisible-v0')
# env = gym.make('CFlatRandGamevarVisible-v0')
# env = gym.make('CCigRandGamevarRwdshapeVisible-v0')
# env = gym.make('CCigRand2GamevarRwdshapeVisible-v0')

# env = gym.make('CCigGamevarVisibleSpectator-v0')
# env = gym.make('CCigGamevarVisible-v0')

# env = gym.make('CFlatcigAttack0GamevarRwdshapeVisibleSpectator-v0')

# env = gym.make('CTestGamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CTestGamevarRwdshapeVisible-v0')
# env = gym.make('CTestGamevarVisibleSpectator-v0')
# env = gym.make('CTest2WuGamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CTest2WuGamevarVisibleSpectator-v0')
# env = gym.make('CTest2GreedyGamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CTest2GreedyGamevarVisibleSpectator-v0')
# env = gym.make('CTest2ProtGreedyGamevarRwdshapeVisibleSpectator-v0')
# env = gym.make('CTest2ProtGreedyGamevarVisibleSpectator-v0')
# env = gym.make('CTest3ProtGreedyGamevarRwdshapeVisibleSpectator-v0')

#################
# track obj
##################
# env = gym.make('TrackObjVisibleSpectator-v2')
# env = gym.make('TrackObjVisible-v2')

#env = gym.make('TrackObjNoiseVisibleSpectator-v2')
#env = gym.make('TrackObjNoise2VisibleSpectator-v2')

#env = gym.make('TrackObjZombieVisibleSpectator-v2')
#env = gym.make('TrackObjMarineVisibleSpectator-v2')
# env = gym.make('TrackObjMarine2VisibleSpectator-v2')
# env = gym.make('TrackObjMonsterxVisibleSpectator-v2')

# env = gym.make('TrackObjFloorCeilVisibleSpectator-v2')

# env = gym.make('TrackObjSmallMaze1VisibleSpectator-v2')
# env = gym.make('TrackObjSmallMazeRandVisible-v2')
# env = gym.make('TrackObjSmallMazeRandFlipVisible-v2')

# env = gym.make('TrackObjOMazeVisibleSpectator-v0')
# env = gym.make('TrackObjOMazeCornerVisibleSpectator-v0')
# env = gym.make('TrackObjCorridorVisibleSpectator-v0')

env = gym.make('TrackObjHardVisible-v2')
#env = gym.make('TrackObjHardVisibleSpectator-v2')
# env = gym.make('TrackObjBackVisibleSpectator-v2')
# env = gym.make('TrackObjBack2VisibleSpectator-v2')
# env = gym.make('TrackObjCounterclockwiseVisibleSpectator-v2')

# env = gym.make('TrackObjShiftVisibleSpectator-v0')
# env = gym.make('TrackObjShiftVisible-v0')

# env = gym.make('TrackObjShiftSlowCloseVisibleSpectator-v0')
# env = gym.make('TrackObjShiftSlowCloseSmallMazeVisibleSpectator-v0')
# env = gym.make('TrackObjShiftSlowCloseSmallMazeRTLVisibleSpectator-v0')
# env = gym.make('TrackObjShiftSlowCloseRDVisibleSpectator-v0')

# env = gym.make('TrackObjVisible-v3')

rand_agent_loop(env, max_episode, sleep_time)